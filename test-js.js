$(function () {
    $("<div class=\"container-fluid p-0 h-100\" id=\"test4_container\"></div>").prependTo(".test4");
    $("<div class=\"d-flex flex-column h-100\" id=\"test4_flex\"></div>").appendTo("#test4_container");
    $(".test4 > .block1").appendTo("#test4_flex");
    $(".test4 > .block2").appendTo("#test4_flex");
    $(".test4 > .block3").appendTo("#test4_flex");
    $("#test4_flex > .block1").addClass("w-50 h-100");
    $("#test4_flex > .block2").addClass("w-50 h-100 align-self-center");
    $("#test4_flex > .block3").addClass("w-50 h-100 align-self-end");

});